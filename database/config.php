<?php
use Illuminate\Database\Capsule\Manager as Capsule;
function connection() {
    $capsule = new Capsule;
    $capsule->addConnection([
        "driver" => "mysql",
        "host" => env('MYSQL_HOST'),
        "database" => env('MYSQL_DATABASE'),
        "username" => env('MYSQL_USERNAME'),
        "password" => env('MYSQL_PASSWORD'),
        'charset' => env('MYSQL_CHARSET'),
        'collation' => env('MYSQL_COLLATION'),
    ]);
    $capsule->setAsGlobal();
    $capsule->bootEloquent();
    return $capsule;
}