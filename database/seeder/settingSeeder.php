<?php
require "../../index.php";

\app\models\Setting::query()->insert([
    ['name' => 'profit_withdraw_fee', 'value' => '4', 'created_at' => date('Y-m-d H:i')],
    ['name' => 'asset_withdraw_fee', 'value' => '5', 'created_at' => date('Y-m-d H:i')],
    ['name' => 'daily_profit', 'value' => '5', 'created_at' => date('Y-m-d H:i')],
    ['name' => 'referral_profit', 'value' => '5', 'created_at' => date('Y-m-d H:i')],
    ['name' => 'start_text', 'value' => "Hi\nWelcome To bot", 'created_at' => date('Y-m-d H:i:s')],
    ['name' => 'wallet_text', 'value' => "Your Wallet Address:", 'created_at' => date('Y-m-d H:i:s')],
    ['name' => 'inventory_text', 'value' => "Your Inventory", 'created_at' => date('Y-m-d H:i:s')],
    ['name' => 'withdraw_text', 'value' => "Enter wallet address", 'created_at' => date('Y-m-d H:i:s')],
    ['name' => 'referral_text', 'value' => "Your referral link", 'created_at' => date('Y-m-d H:i:s')],
    ['name' => 'deposit_min', 'value' => "1500", 'created_at' => date('Y-m-d H:i:s')],
    ['name' => 'withdraw_min', 'value' => "13", 'created_at' => date('Y-m-d H:i:s')],
]);