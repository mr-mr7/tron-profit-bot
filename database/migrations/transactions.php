<?php

require "../../index.php";

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Schema\Blueprint;

Capsule::schema()->create('transactions', function (Blueprint $table) {
    $table->id();
    $table->unsignedBigInteger('user_id');
    $table->float('amount', 20, 8);
    $table->float('fee', 20, 8);
    $table->tinyInteger('type')->comment('-1:withdraw, 1:deposit');
    $table->tinyInteger('status')->default(0)->comment('-100: unsuccessful, 0: new, 100: successful');
    $table->text('des')->nullable();
    $table->json('ext')->nullable();
    $table->timestamps();
    $table->foreign('user_id')
        ->references('user_id')
        ->on('users')
        ->cascadeOnDelete();
});