<?php

require "../../index.php";

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Schema\Blueprint;

Capsule::schema()->create('settings', function (Blueprint $table) {
    $table->id();
    $table->string('name')->nullable();
    $table->text('value')->nullable();
    $table->timestamps();
});