<?php

require "../../index.php";

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Schema\Blueprint;

Capsule::schema()->create('users', function (Blueprint $table) {
    $table->unsignedBigInteger('user_id');
    $table->string('name')->nullable();
    $table->string('wallet_address')->nullable();
    $table->float('asset',20,8)->default(0);
    $table->float('profit',20,8)->default(0);
    $table->float('referral_profit',20,8)->default(0);
    $table->string('step')->default('');
    $table->unsignedBigInteger('parent_id')->nullable();
    $table->tinyInteger('status')->default(0);
    $table->json('ext')->nullable();
    $table->timestamps();


    $table->primary('user_id');

    $table->foreign('parent_id')
        ->references('user_id')
        ->on('users')
        ->nullOnDelete();
});