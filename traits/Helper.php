<?php

namespace app\traits;

use app\lib\Tron;
use app\models\User;
use Telegram\Bot\FileUpload\InputFile;
use Telegram\Bot\Keyboard\Keyboard;
use function Symfony\Component\Translation\t;

trait Helper
{
    private function check_status()
    {

    }

    private function check_join()
    {
        $channels = [];
        $keyboard = Keyboard::make()->inline();
        $join = true;
        foreach ($channels as $item) {
            try {
                $check_join = $this->api->getChatMember(['chat_id' => "@{$item}", 'user_id' => $this->user_id]);
                if (!$check_join || $check_join['status'] == "left" || $check_join['status'] == "kicked") {
                    $join = false;
                    $keyboard->row(Keyboard::inlineButton(['text' => "$item", 'url' => "https://t.me/$item"]));
                }
            } catch (\Exception $e) {

            }
        }
        if ($this->callbak_data && $this->callbak_data == 'check_join') {
            $this->api->deleteMessage(['chat_id' => $this->chat_id, 'message_id' => $this->message_id]);
        }

        if ($join == false) {
            $keyboard->row(Keyboard::inlineButton(['text' => "عضو شدم", 'callback_data' => "check_join"]));
            $text = 'جهت حمایت از ما و باز شدن قفل کانال با استفاده از باتن  زیر، عضو کانال ما شوید، سپس به ربات بازگردید و مجدد /start بزنید ✅';
            $this->api->sendMessage([
                'chat_id' => $this->user_id,
                'text' => "$text",
                'reply_markup' => $keyboard,
                'parse_mode' => 'HTML'
            ]);
            exit("");
        }

    }

    /*
        * $type => اینکه اینلاین کیبورد هست یا کیبورد عادی یا مرحله
        * $val => اینکه چه اینلاینی یا یه کیبوردی یا چه مرحله ای هست
        */
    private function admin_permission($val, $type)

    {
        $permission = [
            'keyboard' => [
                'admin_panel', 'edit_text', 'manage_profit_percent', 'manage_fee', 'manage_min'
            ],
            'inline' => [
                'edit_text', 'manage_profit_percent', 'manage_fee', 'manage_min'
            ],
            'step' => [
                'edit_text', 'manage_profit_percent', 'manage_fee', 'manage_min'
            ],
        ];

        if (in_array($val, $permission["$type"])) {
            if ($this->is_admin != 1) {
                $this->api->triggerCommand('start', $this->updates, ['message' => 'شما به این بخش دسترسی ندارید']);
                exit("");
            }
        }

    }

    public function get_query($key)
    {
        $queries = $this->command_query();
        return $queries["$key"] ?? null;
    }

    private function command_query(): array
    {
        $array = array();
        if (str_starts_with($this->text, '/')) {
            $query = explode(' ', $this->text);
            if (isset($query[1])) {
                $pairs = explode('&', $query[1]);
                foreach ($pairs as $pair) {
                    $temp = explode("=", $pair);
                    $array[$temp[0]] = $temp[1] ?? null;
                }
            }
        }
        return $array;
    }

    private function get_admins()
    {
        return explode(',',env('BOT_ADMINS'));
    }

    private function sendAdmin($type = 'Message', $params = [])
    {
        // حداکثر 30 تا ادمین ارسال میشه
        $admins = $this->get_admins();
        $method = "send{$type}";
        foreach ($admins as $admin) {
            try {
                $this->api->{$method}(array_merge(['chat_id' => $admin], $params));
            } catch (\Exception $exception) {
            }

        }
    }

    private function withdraw($type, $amount)
    {
        $amount= floatval($amount);
        $inventory = $this->user->{$type};
        $withdraw_min= floatval(get_setting('withdraw_min'));
        if ($amount < $withdraw_min) {
            $message= "Min withdraw is: $withdraw_min \n pls try later";
            $this->api->triggerCommand('start', $this->updates, ['message' => $message]);
            exit();
        }

        if ($amount > 0 && $amount <= $inventory) {
            $fee = get_setting("{$type}_withdraw_fee");
            $withdraw_amount = $amount - ($amount * $fee) / 100;

            $transaction = $this->user->transactions()->create([
                'amount' => $withdraw_amount,
                'fee' => $fee,
                'type' => -1, // withdraw
                'status' => 0,
                'des' => "withdraw from $type",
            ]);
            
            
            
            if ($withdraw_amount > 100) {
                $text = "<b>Withdraw Request</b> \n➖➖➖➖➖➖➖➖➖➖ \n";
                $text .= "wallet address: <code>{$this->user->ext['wallet_address']}</code> \n";
                $text .= "amount: $withdraw_amount \n";
                $text .= "user: <a href='tg://user?id={$this->user->user_id}'>{$this->user->user_id}</a> \n";

                $params = [
                    'text' => $text,
                    'parse_mode' => 'HTML'
                ];
                $this->sendAdmin('Message', $params);
                $res['status'] = 'OK';
                $res['data'] = 'send amount by admin';
            } else {
                $network = new Tron();
                $res = $network->Withdraw($this->user->ext['wallet_address'], $withdraw_amount);
            }

            // $tron = new Tron();
            // $res = $tron->Withdraw($this->user->ext['wallet_address'], $withdraw_amount);
            if ($res['status'] == 'OK') {
                $this->user->{$type} = $this->user->{$type} - $amount;
                $this->user->save();

                $transaction->status = 100;
                $transaction->ext = $res['data'];
                $transaction->save();

                $message = 'you withdraw request is successfully !';
                $this->api->triggerCommand('start', $this->updates, ['message' => $message]);
            } else {
                $transaction->status = -100;
                $transaction->ext = $res;
                $transaction->save();

                // send message to admins
                if ($res['errors']['admin']) {
                    $admin_error_text = is_array($res['errors']['admin']) ? '' : $res['errors']['admin'];
                    if (is_array(is_array($res['errors']['admin']))) {
                        foreach ($res['errors']['admin'] as $error) {
                            $admin_error_text .= $error . "\n";
                        }
                    }
                    $params = [
                        'text' => "<b>ERROR!</b> \n➖➖➖➖➖➖➖➖➖➖ \n" . $admin_error_text,
                        'parse_mode' => 'HTML'
                    ];
                    $this->sendAdmin('Message',$params);
                }

                $error_text = is_array($res['errors']['error']) ? '' : $res['errors']['error'];
                if (is_array($res['errors']['error'])) {
                    foreach ($res['errors']['error'] as $error) {
                        $error_text .= $error . "\n";
                    }
                }
                $message = "<b>ERROR! \nPls try agin </b> \n➖➖➖➖➖➖➖➖➖➖ \n" . $error_text;

                $this->api->triggerCommand('start', $this->updates, ['message' => $message]);
            }
        } else {
            $this->api->triggerCommand('start', $this->updates, ['message' => "Your  inventory is not enough!"]);
            exit();
        }
    }
}