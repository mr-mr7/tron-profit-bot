<?php

namespace app\traits;

trait Step
{
    private $step_params;

    public function step_handler()
    {
        if ($this->user->step) {
            $params = explode('#', $this->user->step);
            $method = $params[0];
            if ($method && method_exists(self::class, "step_$method")) {
                $this->admin_permission($method, 'step');
                $this->user->update_step();
                unset($params[0]);
                $this->step_params = array_values($params);
                $this->{"step_$method"}();
                exit();
            }
        }
    }

    private function step_wallet_address()
    {
        if (!str_starts_with($this->text,'T') || strlen($this->text) != 34) {
            $this->user->update_step('wallet_address');
            $text = "<b>Wallet address is wrong!</b> \n\n".get_setting('withdraw_text');
            $this->callback_withdraw($text);
            exit();
        }
        $this->user->ext= ['wallet_address' => $this->text];
        $this->user->save();

        $text = "Choose an option "."\n";
        $text .= "➖➖➖➖➖➖➖➖➖ \n";
        $text .= "<b>asset: </b> this way you can input withdraw amount from <b>asset</b> inventory \n";
        $text .= "<b>profit: </b> this way you withdraw all <b>profit</b> inventory \n\n";

        $inline_keyboard = json_encode([
            'inline_keyboard' => [
                [
                    ['text' => 'Withdraw Asset', 'callback_data' => 'withdraw#asset'],
                    ['text' => 'Withdraw Profit', 'callback_data' => 'withdraw#profit']
                ],
            ]
        ]);

        $this->api->sendMessage([
            'chat_id' => $this->chat_id,
            'text' => $text,
            'reply_markup' => $inline_keyboard,
            'parse_mode' => 'HTML'
        ]);
    }

    public function step_withdraw()
    {
        $this->withdraw('asset', floatval($this->text));
    }

    // ================= ADMIN ================
    protected function step_edit_text()
    {
        update_setting("{$this->step_params[0]}_text", $this->text);
        $this->api->sendMessage([
            'chat_id' => $this->chat_id,
            'text' => "update successfully!",
        ]);
        $this->callback_edit_text();
    }

    protected function step_manage_profit_percent()
    {
        update_setting("{$this->step_params[0]}_profit", floatval($this->text));
        $this->api->sendMessage([
            'chat_id' => $this->chat_id,
            'text' => "update successfully!",
        ]);
        $this->callback_manage_profit_percent();
    }

    protected function step_manage_fee()
    {
        update_setting("{$this->step_params[0]}_fee", floatval($this->text));
        $this->api->sendMessage([
            'chat_id' => $this->chat_id,
            'text' => "update successfully!",
        ]);
        $this->callback_manage_fee();
    }

    protected function step_manage_min()
    {
        update_setting("{$this->step_params[0]}_min", floatval($this->text));
        $this->api->sendMessage([
            'chat_id' => $this->chat_id,
            'text' => "update successfully!",
        ]);
        $this->callback_manage_min();
    }
}