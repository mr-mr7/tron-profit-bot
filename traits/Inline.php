<?php

namespace app\traits;

use app\models\User;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Laravel\Facades\Telegram;
use function Symfony\Component\Translation\t;

trait Inline
{
    private $inline_params;

    public function inline_handler()
    {
        if ($this->callbak_data) {
            $params = explode('#', $this->callbak_data);
            $method = $params[0];
            if ($method && method_exists(self::class, "inline_$method")) {
                $this->admin_permission($method, 'inline');
                $this->user->update_step();
                unset($params[0]);
                $this->inline_params = array_values($params);
                $this->{"inline_$method"}();
                $this->api->answerCallbackQuery(['callback_query_id' => $this->callbak_id,]);
                exit();
            }
        }
    }

    private function inline_withdraw()
    {
        $withdraw_type = $this->inline_params[0]; // asset, profit
        if ($withdraw_type == 'asset') {
            $text= 'The withdrawals will be available 30 days after investment time...';
            $this->api->sendMessage([
                'chat_id' => $this->chat_id,
                'text' => $text,
                'parse_mode' => 'HTML'
            ]);
            // $this->api->sendMessage([
            //     'chat_id' => $this->chat_id,
            //     'text' => 'Enter withdraw amount',
            //     'parse_mode' => 'HTML'
            // ]);
            // $this->user->update_step('withdraw');
        } else if ($withdraw_type == 'profit') {
            $this->withdraw($withdraw_type, $this->user->profit);
        }
    }

    private function inline_return()
    {
        if ($this->inline_params[0] == 'main_menu') {
            $this->api->triggerCommand('start', $this->updates);
        }
    }

    // ============== ADMIN ==================
    private function inline_edit_text()
    {
        $text= "Enter new text \n";
        $text .= "➖➖➖➖➖➖➖➖➖ \n";
        $text .= "Old text: \n";
        $text .= get_setting("{$this->inline_params[0]}_text");
        $this->api->sendMessage([
            'chat_id' => $this->chat_id,
            'text' => $text,
            'parse_mode' => 'HTML'
        ]);
        $this->user->update_step("edit_text#{$this->inline_params[0]}");
    }

    private function inline_manage_profit_percent()
    {
        $text= "Enter new percent \n";
        $text .= "➖➖➖➖➖➖➖➖➖ \n";
        $text .= "Old percent: \n";
        $text .= get_setting("{$this->inline_params[0]}_profit");
        $this->api->sendMessage([
            'chat_id' => $this->chat_id,
            'text' => $text,
            'parse_mode' => 'HTML'
        ]);
        $this->user->update_step("manage_profit_percent#{$this->inline_params[0]}");
    }

    private function inline_manage_fee()
    {
        $text= "Enter new fee \n";
        $text .= "➖➖➖➖➖➖➖➖➖ \n";
        $text .= "Old fee: \n";
        $text .= get_setting("{$this->inline_params[0]}_fee");
        $this->api->sendMessage([
            'chat_id' => $this->chat_id,
            'text' => $text,
            'parse_mode' => 'HTML'
        ]);
        $this->user->update_step("manage_fee#{$this->inline_params[0]}");
    }

    private function inline_manage_min()
    {
        $text= "Enter new min \n";
        $text .= "➖➖➖➖➖➖➖➖➖ \n";
        $text .= "Old min: \n";
        $text .= get_setting("{$this->inline_params[0]}_min");
        $this->api->sendMessage([
            'chat_id' => $this->chat_id,
            'text' => $text,
            'parse_mode' => 'HTML'
        ]);
        $this->user->update_step("manage_min#{$this->inline_params[0]}");
    }
}