<?php

namespace app\traits;

use app\lib\Tron;
use app\models\Transaction;
use Telegram\Bot\Keyboard\Keyboard;

trait Callback
{
    public function callback_handler()
    {
        $method = $this->keyboard_key($this->text);
        if ($method && method_exists(self::class, "callback_$method")) {
            $this->admin_permission($method, 'keyboard');
            $this->user->update_step();
            $this->api->sendChatAction([
                'chat_id' => $this->chat_id,
                'action' => 'typing',
            ]);
            $this->{"callback_$method"}();
            exit();
        }
    }

    private function callback_main_menu()
    {
        $this->api->triggerCommand('start', $this->updates);
    }

    private function callback_wallet_address()
    {
        $text = get_setting('wallet_text') . "\n";
        $text .= "<code>{$this->user->wallet_address}</code> \n";
        $this->api->sendMessage([
            'chat_id' => $this->chat_id,
            'text' => $text,
            'parse_mode' => 'HTML'
        ]);
    }

    private function callback_inventory()
    {
        // check deposit
        $res = (new Tron())->DepositCheck($this->user->wallet_address, $this->user->user_id);
        if ($res && $res['status'] == 'OK') {
            if ($res['data']['transactions'] && count($res['data']['transactions']) > 0) {
                $transactions = [];
                foreach ($res['data']['transactions'] as $item) {
                    $transactions[] = [
                        'user_id' => "{$this->user->user_id}",
                        'amount' => $item['value'],
                        'fee' => 0,
                        'type' => 1, // deposit
                        'status' => 0,
                        'des' => "",
                        'ext' => json_encode($item),
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ];
                }
                Transaction::query()->insert($transactions);
            }
        }

        $text = get_setting('inventory_text') . "\n";
        $text .= "➖➖➖➖➖➖➖➖➖ \n";
        $text .= "Asset Inventory: <b>{$this->user->asset}</b> \n";
        $text .= "Profit Inventory: <b>{$this->user->profit}</b> \n";
        $text .= "Referral Profit: <b>{$this->user->referral_profit}</b> \n";

        $this->api->sendMessage([
            'chat_id' => $this->chat_id,
            'text' => $text,
            'parse_mode' => 'HTML'
        ]);

    }

    private function callback_withdraw($text = '')
    {
        $text = $text ?: get_setting('withdraw_text');
        $inline_keyboard = json_encode([
            'inline_keyboard' => [
                [
                    ['text' => 'Return to main menu', 'callback_data' => 'return#main_menu'],
                ],
            ]
        ]);
        $this->api->sendMessage([
            'chat_id' => $this->chat_id,
            'text' => $text,
            'reply_markup' => $inline_keyboard,
            'parse_mode' => 'HTML'
        ]);

        $this->user->update_step('wallet_address');
    }

    private function callback_referral_link()
    {
        $text = get_setting('referral_text') . "\n";
        $text .= "➖➖➖➖➖➖➖➖➖ \n";
        $text .= "<code>{$this->user->referral_link}</code>";
        $this->api->sendMessage([
            'chat_id' => $this->chat_id,
            'text' => $text,
            'parse_mode' => 'HTML'
        ]);
    }


    //================= ADMIN =======================
    private function callback_admin_panel()
    {

        $keyboard = [
            [$this->keyboard_text('edit_text')],
            [$this->keyboard_text('manage_profit_percent'), $this->keyboard_text('manage_fee')],
            [$this->keyboard_text('manage_min')],
            [$this->keyboard_text('main_menu')],
        ];

        $reply_markup = Keyboard::make([
            'keyboard' => $keyboard,
            'resize_keyboard' => true,
        ]);

        $this->api->sendMessage([
            'chat_id' => $this->chat_id,
            'text' => 'welcome to Admin Panel',
            'reply_markup' => $reply_markup,
            'parse_mode' => 'HTML'
        ]);
    }

    private function callback_edit_text()
    {
        $text = "Choose text to edit\n";

        $inline_keyboard = json_encode([
            'inline_keyboard' => [
                [
                    ['text' => 'Start text', 'callback_data' => 'edit_text#start'],
                    ['text' => 'Wallet text', 'callback_data' => 'edit_text#wallet']
                ],
                [
                    ['text' => 'Inventory text', 'callback_data' => 'edit_text#inventory'],
                    ['text' => 'Withdraw text', 'callback_data' => 'edit_text#withdraw']
                ],
                [
                    ['text' => 'Referral text', 'callback_data' => 'edit_text#referral'],
//                    ['text' => 'Wallet address text', 'callback_data' => 'edit_text#input_wallet_address'],
                ],
            ]
        ]);

        $this->api->sendMessage([
            'chat_id' => $this->chat_id,
            'text' => $text,
            'reply_markup' => $inline_keyboard,
            'parse_mode' => 'HTML'
        ]);
    }

    private function callback_manage_profit_percent()
    {
        $text = "Choose item to change profit percent\n";

        $inline_keyboard = json_encode([
            'inline_keyboard' => [
                [
                    ['text' => 'Referral profit', 'callback_data' => 'manage_profit_percent#referral'],
                    ['text' => 'Daily profit', 'callback_data' => 'manage_profit_percent#daily']
                ]
            ]
        ]);

        $this->api->sendMessage([
            'chat_id' => $this->chat_id,
            'text' => $text,
            'reply_markup' => $inline_keyboard,
            'parse_mode' => 'HTML'
        ]);
    }

    private function callback_manage_fee()
    {
        $text = "Choose item to change fee\n";

        $inline_keyboard = json_encode([
            'inline_keyboard' => [
                [
                    ['text' => 'Asset withdraw fee', 'callback_data' => 'manage_fee#asset_withdraw'],
                    ['text' => 'Profit withdraw fee', 'callback_data' => 'manage_fee#profit_withdraw']
                ]
            ]
        ]);

        $this->api->sendMessage([
            'chat_id' => $this->chat_id,
            'text' => $text,
            'reply_markup' => $inline_keyboard,
            'parse_mode' => 'HTML'
        ]);
    }

    private function callback_manage_min()
    {
        $text = "Choose item to change min\n";

        $inline_keyboard = json_encode([
            'inline_keyboard' => [
                [
                    ['text' => 'Min deposit', 'callback_data' => 'manage_min#deposit'],
                    ['text' => 'Min Withdraw', 'callback_data' => 'manage_min#withdraw'],
                ]
            ]
        ]);

        $this->api->sendMessage([
            'chat_id' => $this->chat_id,
            'text' => $text,
            'reply_markup' => $inline_keyboard,
            'parse_mode' => 'HTML'
        ]);
    }

}