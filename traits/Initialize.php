<?php

namespace app\traits;

use app\commands\StartCommand;
use app\lib\Tron;
use app\models\User;
use Hashids\Hashids;

trait Initialize
{
    use Callback, Inline, Step, Helper;

    protected $user_id, $chat_id, $message_id, $text, $user, $callbak_data, $callbak_id, $en_id, $full_name, $updates, $is_admin = 0;

    private function set_attributes($updates = [])
    {
        $this->updates = empty($updates) ? $this->api->getWebhookUpdate() : $updates;

        if ($this->updates->has('callback_query')) { // inline button
            $this->user_id = "{$this->updates->callback_query['from']['id']}";
            $this->en_id = $this->updates->callback_query['from']['username'];
            $this->full_name = $this->updates->callback_query['from']['first_name'] ?? '' . $this->updates->callback_query['from']['last_name'] ?? '';
            $this->chat_id = "{$this->updates->callback_query['message']['chat']['id']}";
            $this->message_id = "{$this->updates->callback_query['message']['message_id']}";
            $this->text = $this->updates->callback_query['message']['text'];

            $this->callbak_data = $this->updates->callback_query['data'];
            $this->callbak_id = "{$this->updates->callback_query['id']}";
        } else if ($this->updates->has('message')) {
            $this->user_id = "{$this->updates->message['from']['id']}";
            $this->en_id = $this->updates->message['from']['username'];
            $this->full_name = $this->updates->message['from']['first_name'] ?? '' . $this->updates->message['from']['last_name'] ?? '';
            $this->chat_id = "{$this->updates->message['chat']['id']}";
            $this->message_id = "{$this->updates->message['message_id']}";
            $this->text = $this->updates->message['text'];
        } else {
            exit("");
        }

        if (in_array($this->user_id, $this->get_admins())) {
            $this->is_admin = 1;
        }

    }

    private function keyboard_text($key)
    {
        $array = $this->keyboards_array();
        return $array[$key] ?? '';
    }

    private function keyboard_key($text)
    {
        $array = array_flip($this->keyboards_array());
        return $array[$text] ?? '';
    }

    private function set_user()
    {
        $parent = null;
        if ($this->get_query('referral')) {
            $parent_id = (new Hashids())->decode($this->get_query('referral'))[0];
            // $this->api->sendMessage([
            //     'chat_id' => $this->chat_id,
            //     'text' => "parent id: ".$this->get_query('referral')." ## ".$parent_id,
            //     'parse_mode' => 'HTML'
            // ]);
            $parent = User::query()->where('user_id', "$parent_id")->first();
        }
        $this->user = User::query()->firstOrCreate(
            ['user_id' => "{$this->user_id}"],
            ['name' => $this->full_name, 'parent_id' => $parent?"{$parent->user_id}":null]
        );

        if (is_null($this->user->wallet_address)) {
            $res = (new Tron())->CreateWallet($this->user_id);
            if ($res['status'] == 'OK') {
                $this->user->wallet_address = $res['data']['address'];
                $this->user->save();
            }
        }
    }

    private function keyboards_array()
    {
        return [
            'wallet_address' => 'Your Wallet Address',
            'inventory' => 'Inventory',
            'withdraw' => 'Withdraw',
            'referral_link' => 'Referral Link',
            'admin_panel' => 'Admin panel',

            //admin
            'edit_text' => 'Edit bot text',
            'manage_profit_percent' => 'Manage profit percent',
            'manage_fee' => 'Manage fee',
            'manage_min' => 'Manage Min',
            'main_menu' => 'Return to main menu',

        ];
    }

    private function commands()
    {
        $this->api->addCommands([
            StartCommand::class,
        ]);
    }

}