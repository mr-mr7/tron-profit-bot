<?php

namespace app\models;

use Hashids\Hashids;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $guarded = [];
    protected $casts= [
        'ext' => 'json'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'user_id');
    }

    public function scopeDeposit($q)
    {
        return $q->where('type',1);
    }

    public function scopeWithdraw($q)
    {
        return $q->where('type',-1);
    }

    public function scopeNew($q)
    {
        return $q->where('status',0);
    }

    public function scopeSuccess($q)
    {
        return $q->where('status',100);
    }

    public function scopeUnsuccessful($q)
    {
        return $q->where('status',-100);
    }
}