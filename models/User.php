<?php

namespace app\models;

use Hashids\Hashids;
use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $guarded = [];

    protected $primaryKey = 'user_id';
    protected $keyType = 'string';

    protected $casts=[
        'ext' => 'json',
        'user_id' => 'string'
    ];

    public function transactions()
    {
        return $this->hasMany(Transaction::class,'user_id','user_id');
    }

    public function parent()
    {
        return $this->belongsTo(User::class,'parent_id','user_id');
    }

    public function subset()
    {
        return $this->hasMany(User::class,'parent_id','user_id');
    }

    public function update_step($step = '')
    {
        $this->update(['step' => "$step"]);
    }

    public function getReferralLinkAttribute()
    {
        $bot_username= bot_username();
        // برای این دوبار ایدی رو نوشتم که طول رشته رفرال حداقل 4 رقم باشه
        return "https://t.me/$bot_username?start=referral=".(new Hashids())->encode($this->user_id);
    }


}