<?php

require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/database/config.php';

use app\core\Telegram;
use Telegram\Bot\Api;

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

$capsule = connection();

$bot_token = env('BOT_TOKEN');
if (isset($_GET['token']) && $_GET['token'] == $bot_token) {
    $api = new Api("$bot_token");
    $telegram = new Telegram($api);
    $telegram->handler();
}

//$res= (new \app\lib\Tron())->TxHashCheck('f6265a8663126856ca5f686572cefee7c61202fb25998b6a78d1002c48899298');
//dd($res);
// این قسمت پایین دستی هست و خودمون کامنت و انکامنتش میکنیم
// $telegram = new Api("$bot_token");
// $bot_url= env('BOT_URL');
// echo $telegram->getMe();
// echo $telegram->getWebhookInfo();
// echo $telegram->deleteWebhook();
// echo $telegram->setWebhook(['url' => "$bot_url?token=$bot_token"]);