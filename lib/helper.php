<?php

use app\models\Setting;

function dd(...$args)
{
    foreach ($args as $arg) {
        echo "<pre>";
        var_dump($arg);
        echo "</pre>";
    }
    exit();
}

function get_setting($name, $first = true)
{
    if ($first == true) {
        return Setting::query()->where('name', "$name")->value('value');
    } else {
        return Setting::query()->where('name', "$name")->get();
    }
}

function update_setting($name, $value)
{
    return Setting::query()->updateOrCreate(['name' => "$name"], ['value' => "$value"]);
}

function bot_username() {
    global $api;
    $bot = $api->getMe();
    return $bot['username'];
}

function check_admin($user_id) {
    global $telegram;
    if (in_array($user_id,$telegram->get_admins())) {
        return true;
    }
}