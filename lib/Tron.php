<?php

namespace app\lib;


use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;

class Tron
{
    private $client;

    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => 'http://staking.robattelegram.com/',
            'headers' => [
                'x-auth-token' => 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6ImVzdDMzMjUyNDUiLCJuYW1lIjoiQWRtaW4iLCJpYXQiOjE1MTYyMzkwMjJ9.CBFervm6imC_Uoj9ZBSu1ljRkayJ19-59-Ob82lwBlc'
            ]
        ]);
    }

    public function CreateWallet($user_id, $blockchain = 'Tron')
    {
        try {
            $response = $this->client->post('createWallet', [
                'form_params' => [
                    'blockchain' => $blockchain,
                    'user_id' => $user_id,
                ]
            ]);
            return $this->response($response);
        } catch (ServerException|ClientException $exception) {
            return $this->response($exception);
        }
    }

    public function DepositCheck($address, $user_id, $blockchain = 'Tron')
    {
        try {
            $response = $this->client->post('checkDepositTron', [
                'form_params' => [
                    'blockchain' => $blockchain,
                    'address' => $address,
                    'user_id' => $user_id,
                ]
            ]);
            return $this->response($response);
        } catch (ServerException|ClientException $exception) {
            return $this->response($exception);
        }

    }

    public function TxHashCheck($hash, $blockchain = 'Tron')
    {
        try {
            $response = $this->client->post('checkTxHash', [
                'form_params' => [
                    'hash' => $hash,
                    'blockchain' => $blockchain,
                ]
            ]);
            return $this->response($response);
        } catch (ServerException|ClientException $exception) {
            return $this->response($exception);
        }
    }

    public function Withdraw($address, $amount, $currency = 'TRX', $blockchain = 'Tron')
    {

        try {
            $response = $this->client->post('withdraw', [
                'form_params' => [
                    'amount' => $amount,
                    'address' => $address,
                    'currency' => $currency
                ]
            ]);
            return $this->response($response);
        } catch (ServerException|ClientException $exception) {
            return $this->response($exception);
        }
    }

    private function response($response)
    {
        $response = $response instanceof ServerException || $response instanceof ClientException ? $response->getResponse() : $response;
        return $response ? json_decode($response->getBody()->getContents(), true) : false;
    }
}