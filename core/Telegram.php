<?php

namespace app\core;

use app\traits\Callback;
use app\traits\Helper;
use app\traits\Initialize;
use app\traits\Inline;
use app\traits\Step;
use Telegram\Bot\Api;

class Telegram
{
//    use Callback, Inline, Step, Initialize, Helper;
    use Initialize;

    private Api $api;

    public function __construct($api)
    {
        $this->api = $api;
    }

    public function handler()
    {
        $this->set_attributes();
        $this->set_user();

        $this->check_status();
        $this->check_join();

        $this->commands();
        $this->api->commandsHandler(true);

        $this->callback_handler();
        $this->inline_handler();
        $this->step_handler();
    }


}