<?php

namespace app\commands;

use app\models\User;
use app\traits\Initialize;
use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\Keyboard\Keyboard;

class StartCommand extends Command
{
    use Initialize;

    /**
     * @var string Command Name
     */
    protected $name = "start";

    /**
     * @var string Command Description
     */
    protected $description = "وقتی کاربر ربات رو استارت کرد";

    /**
     * @inheritdoc
     */
    public function handle()
    {
        $updates = $this->getUpdate();
        $this->set_attributes($updates);

        $user_id = "{$updates->message['from']['id']}";

        $this->replyWithChatAction(['action' => Actions::TYPING]);

        $text = $this->entity['message'] ?? get_setting('start_text');

        $keyboard = [
            [$this->keyboard_text('wallet_address'), $this->keyboard_text('inventory')],
            [$this->keyboard_text('withdraw'), $this->keyboard_text('referral_link')],
        ];

        if ($this->is_admin) {
            array_unshift($keyboard, [$this->keyboard_text('admin_panel')]);
        }

        $reply_markup = Keyboard::make([
            'keyboard' => $keyboard,
            'resize_keyboard' => true,
        ]);

        
        $this->replyWithPhoto([
            'photo' => \Telegram\Bot\FileUpload\InputFile::create(__DIR__.'/../files/images/start.jpg', 'start.jpg'),
            'caption' => $text,
            'reply_markup' => $reply_markup,
            'parse_mode' => 'HTML'
        ]);

        // $this->replyWithMessage([
        //     'text' => $text,
        //     'reply_markup' => $reply_markup,
        //     'parse_mode' => 'HTML'
        // ]);

        User::query()->where('user_id', $user_id)->update(['step' => '']);

    }
}