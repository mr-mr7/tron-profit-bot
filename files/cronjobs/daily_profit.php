<?php

use app\models\User;
use Illuminate\Database\Capsule\Manager;

require __DIR__ . "/../../index.php";

$daily_profit = get_setting('daily_profit');
if ($daily_profit <= 0) return;

$deposit_min = get_setting('deposit_min');
User::query()
    ->where('asset', '>=', $deposit_min)
    ->update([
        'profit' => Manager::raw("users.profit + (users.asset * $daily_profit/100)"),
    ]);