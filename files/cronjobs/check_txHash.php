<?php

use app\models\Transaction;
use app\models\User;
use Illuminate\Database\Capsule\Manager;

require __DIR__."/../../index.php";

$tron = new \app\lib\Tron();
$transactions = Transaction::query()->deposit()->new()->get();
$referral_profit = get_setting('referral_profit');
foreach ($transactions as $tran) {
    $res = $tron->TxHashCheck($tran->ext['tx_hash']);
    if ($res['status'] == 'OK') {
        if ($res['data']['status'] == 'success') {
            $tran->status = 100;
            $tran->save();

            $user = $tran->user;
            $user->update([
                'asset' => Manager::raw("asset + {$tran->amount}")
            ]);

            // اگر موجودی والد بیشتر یا مساوی حداقل واریز بود پاداش ریفرال بده
            $deposit_min= get_setting('deposit_min');
            if ($user->parent->asset >= $deposit_min) {
                $profit= $tran->amount * $referral_profit / 100;
                $res= $user->parent()->update([
                    'profit' => Manager::raw("profit + {$profit}"),
                    'referral_profit' => Manager::raw("referral_profit + {$profit}"),
                ]);
                if ($res) {
                    Transaction::query()->create([
                        'user_id' => "{$user->parent_id}",
                        'amount' => $profit,
                        'fee' => 0,
                        'type' => 1, // deposit
                        'status' => 100,
                        'ext' => ['user_id' => $user->user_id, 'deposit_amount' => $tran->amount],
                        'des' => "referral profit",
                    ]);
                }
            }
        }
    }
}
